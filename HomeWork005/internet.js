var btnClick = document.getElementById("button");
btnClick.innerHTML = "Start";

btnClick.addEventListener("click", () => {
  var start = document.getElementById("starts");
  var stop = document.getElementById("stops");
  var minute = document.getElementById("minutes");
  var money = document.getElementById("moneys");

  if (btnClick.innerHTML === "Start") {
    btnClick.innerHTML = "Stop";
    btnClick.classList.remove("start");
    btnClick.classList.remove("clear");
    btnClick.classList.add("stop");
  
    const d = new Date();
    const hourStart = Number(d.getHours() %12 ||12 );
    const minutesStart = Number(d.getMinutes());
    start.innerHTML = ` Start At: ${hourStart} : ${minutesStart} `;
  } else if (btnClick.innerHTML === "Stop") {
    btnClick.innerHTML = "Clear";
    btnClick.classList.remove("start");
    btnClick.classList.remove("stop");
    btnClick.classList.add("clear");
 
    const d = new Date();
    const hourStop = Number(d.getHours()%12 ||12);
    const minutesStop = Number(d.getMinutes());
    const startHours = Number((start.innerHTML.split(":")[1]));
    const startMinutes = Number((start.innerHTML.split(":")[2]));
    const totalHour = (hourStop - startHours) * 60;
    const totalMinute = (minutesStop - startMinutes);
    const total = (totalHour + totalMinute);
    
    stop.innerHTML = `Stop At: ${hourStop}: ${minutesStop} `;
    minute.innerHTML = `${total} Minutes(s)`;
    if(total >= 0 && total <= 15 ) {
        if(total == 0) {
          money.innerHTML = `500 Real(s)`;
        } else {
          money.innerHTML = `${total * 500} Real(s)`;
        }
    }
    if(total > 15 && total <= 30 ) {
      money.innerHTML = `${total * 1000} Real(s)`;
    } 
    if(total > 30 && total <= 60) {
      money.innerHTML = `${total * 1500} Real(s)`;
    }
    if(total > 60) {
      money.innerHTML = `${total * 2000} Real(s)`;
    }
  } else {
    btnClick.innerHTML = "Start";
    btnClick.classList.remove("clear");
    btnClick.classList.remove("stop");
    btnClick.classList.add("start");
 
    start.innerHTML = "Start At: 0 h: 00 mn";
    stop.innerHTML = "Stop At: 0 h: 00mn";
    minute.innerHTML = "0 Minutes(mn)";
    money.innerHTML = "0 Real(s)";
  }
});

function renderTime() {
  var dateTime = new Date();
  var year = dateTime.getYear();
  if(year < 1000) {
    year += 1900
  }
  var day = dateTime.getDay();
  var month = dateTime.getMonth();
  var daym = dateTime.getDate();
  var days = ["Sunday", "Monday", "TuesDay", "Wednesday", "Thursday", "Friday", "Saturday"];
  var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  
  var currentTime = new Date();
  var h = currentTime.getHours();
  var m = currentTime.getMinutes();
  var s = currentTime.getSeconds();
  let session = "AM";
  if(h == 0) {
    h = 12;
  } else if(h > 12) {
    h = h-12;
    session= "PM";
  }
  if(h < 10) {
    h = "0" + h;
  }
  if(m < 10) {
    m = "0" + m;
  }
  if(s < 10) {
    s = "0" + s;
  }
  var footer = document.getElementById("footer");
  footer.innerHTML = `${days[day]} ${daym} ${months[month]} ${year} || ${h} : ${m} : ${s}` + " "+session;
  setTimeout("renderTime()", 1000);
}
